var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var path = require('path');

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin,X-Requested-Width-Content-Type,Accept");
    next();
});

var requestjson = require('request-json');

var urlClientes = "https://api.mlab.com/api/1/databases/gdiaz/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var bodyParser = require('body-parser');
app.use(bodyParser.json());
var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
    res.sendfile(path.join(__dirname, 'index.html'));
});

app.get('/Clientes/:idcliente', function(req, res) {
    res.send('Aqui tiene al cliente no: ' + req.params.idcliente);
});

app.post('/', function(req, res) {
    res.send('Hemos recibido su petición POST');
});

app.put('/', function(req, res) {
    res.send('Hemos recibido su petición PUT bien');
});

app.delete('/', function(req, res) {
    res.send('Hemos recibido su petición DELETE');
});

app.get('/v1/movimientos', function(req, res) {
    res.sendfile('movimientosv1.json');
});

app.get('/v2/movimientos', function(req, res) {
    res.json(movimientosJSON);
});

app.get('/v2/movimientos/:indice', function(req, res) {
    console.log(req.params.indice);
    res.json(movimientosJSON[req.params.indice]);
});

app.get('/v3/movimientosquery', function(req, res) {
    console.log(req.query);
    res.send('recibido');
});

app.post('/v3/movimientos', function(req, res) {
    var nuevo = req.body;
    nuevo.id = movimientosJSON.length + 1;
    movimientosJSON.push(nuevo);
    res.send('Movimiento dado de alta');
});

// NUESTRO MONGODB

app.get('/Clientes', function(req, res) {
    var clienteMLab = requestjson.createClient(urlClientes);
    clienteMLab.get('', function(err, resM, body) {
        if (err) {
            console.log(body);
        } else {
            res.send(body);
        }
    })
});