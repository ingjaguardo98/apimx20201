#Image base
FROM node:latest

#Directorio de la aplicacion
WORKDIR /app

#Copidado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comando
CMD ["npm", "start"]